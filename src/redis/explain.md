###redis锁类 
###MredisLock
####方法：
gettLock 获取锁
delLock 删除锁(释放)
delKey  删除键
###redis距离类 
###MredisDistance(redis版本必须3.2以上)
geoAdds  设置二维数组元素
geoAdd 设置一维数组元素
geoDist 求两标识的距离
geoRadius 返回经纬度最大距离的数据
geoRadiusByMember 求标识经纬度最大距离的数据 
geoDel 删除某个键名
delKey  删除键
###redis排行榜
###MredisRanking
Adds 添加元素多个
Add 添加元素一个
getRanking 获取排行榜前$count位
getMemberRanking 获取用户的排行
getMemberRange 获取用户附近的用户
delKey  删除键


