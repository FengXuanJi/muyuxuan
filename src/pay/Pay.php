<?php
namespace Maowenke\pay;
use Alipay\EasySDK\Kernel\Config;
use EasyWeChat\Factory;
use Maowenke\pay\alipay\Alipay;
use Maowenke\pay\trilateral\Trilateral;
use Maowenke\pay\wechat\WechatOther;

class Pay
{
    protected $config = [];//配置
    protected $PayObj = null;//支付对象
    protected $type = null;//支付类型wechat=微信,alipay=支付宝,trilateral=三方
    protected $trade_type = null;//微信支付方式
    public function __construct($config=[]){
        if(!empty($config)){
            $this->config = $config;
        }
    }
    use WechatOther;
    use Alipay;
    use Trilateral;
    use Other;
    /**
     * 官方微信支付
     * @param array $config 微信配置
     * @return Pay
     */
    public static function wechat(array $config){
        if(empty($config)){
            return false;
        }
        $obj = new self;
        $obj->PayObj = Factory::payment($config);
        $obj->config = $config;
        $obj->type = 'wechat';
        return $obj;
    }

    /**
     * 支付宝官方支付
     * @param array $config
     * @return false|Pay
     */
    public static function alipay(array $config){
        if(empty($config)){
            return false;
        }
        $aliConfig = new Config();
        foreach ($config as $key=>$v){
            $aliConfig->$key = $v;
        }
        if(empty($aliConfig->appId)){
            $aliConfig->appId = $config['app_id'];
        }
        if(empty($aliConfig->protocol)){
            $aliConfig->protocol = 'https';
        }
        if(empty($aliConfig->gatewayHost)){
            $aliConfig->gatewayHost = 'openapi.alipay.com';
        }
        if(empty($aliConfig->signType)){
            $aliConfig->signType = 'RSA2';
        }
        \Alipay\EasySDK\Kernel\Factory::setOptions($aliConfig);
        $obj = new self;
        $obj->config = $config;
        $obj->type = 'alipay';
        return $obj;
    }

    /**
     * @param array $config
     * @return Pay
     */
    public static function trilateral(array $config){
        $obj = new self;
        $obj->config = $config;
        $payType = (empty($config['payType'])?"PayTrilateral":$config['payType']);
        $objType = empty($config['objType'])?"hu":$config['objType'];
        $file = __DIR__.DIRECTORY_SEPARATOR.'trilateral'.DIRECTORY_SEPARATOR.$objType.DIRECTORY_SEPARATOR.$payType.'.php';
        if(!is_file($file)){
            return false;
        }
        $className = "Maowenke\pay\\trilateral\\".$objType."\\".$payType;
        $obj->PayObj = $obj->StrinToObj($className,empty($config['config'])?[]:$config['config']);
        return $obj;
    }

    /**创建支付对象
     * @param array $data
     * @return false
     */
    public function createCharge(array $data){
        $method = 'createCharge';
        if(!method_exists($this->PayObj,$method)){
            $this->message = '类方法不存在';
            return false;
        }
        $res = $this->PayObj->createCharge($data);
        if($res===false){
            $this->message = $this->PayObj->getMessage();
            return false;
        }
    }

    /**
     * 支付接口
     * @param array $data 支付数据
     * @return false
     */
    public function unify(string $out_trade_no,float $total_fee,string $notify_url,string $payType='H5',array $event=[]){
        $data['out_trade_no'] = $out_trade_no;
        $data['total_fee'] = $total_fee;
        $data['notify_url'] = $notify_url;
        $data['pay_type'] = strtolower($payType);
        $data = array_merge($data,$event);
        if(empty($this->PayObj)){
            $this->message = "支付对象不存在";
            return false;
        }
        if(empty($data)){
            $this->message = "传入数据不存在";
            return false;
        }
        if(isset($data['pay_type'])){
            $data['pay_type'] = strtolower($data['pay_type']);
        }
        //微信支付
        if($this->type=='wechat'){
            return $this->wechatUnify($data);
        }elseif($this->type=='alipay'){
            return $this->MyAliPay($data);
        }else{
            return $this->TrilateralUnify($data);
        }
    }

    /**
     * 根据商户订单号查询
     * @param string $out_trade_no
     * @return mixed
     */
    public function queryByOutNumber(string $out_trade_no){
        if($this->type=='wechat'){
            return $this->PayObj->order->queryByOutTradeNumber($out_trade_no);
        }
    }

    /**
     * 根据订单号查询
     * @param string $transaction_id
     * @return mixed
     */
    public function queryByNumber(string $transaction_id){
        if($this->type=='wechat'){
            return $this->PayObj->order->queryByTransactionId($transaction_id);
        }
    }

    /**
     * 回调
     * @param function $callback 回调函数
     * @return mixed
     */
    public function notify($callback){
        if($this->type=='wechat'){
            $respons = $this->PayObj->handlePaidNotify($callback);
            return $respons->send();
        }
    }

    /**
     * 关闭订单
     * @param string $out_trade_no
     * @return mixed
     */
    public function close(string $out_trade_no){
        if($this->type=='wechat'){
            return $this->PayObj->order->close($out_trade_no);
        }
    }
}