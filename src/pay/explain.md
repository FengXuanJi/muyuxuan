###微信支付使用的数据
```aidl
//微信官方支付配置

$config = [
    // 必要配置
    'app_id'             => 'xxxx',
    'secret'             => 'xxxx',
    'mch_id'             => 'your-mch-id',
    'key'                => 'key-for-signature',   // API v2 密钥 (注意: 是v2密钥 是v2密钥 是v2密钥)

    // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
    'cert_path'          => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！
    'key_path'           => 'path/to/your/key',      // XXX: 绝对路径！！！！

    'notify_url'         => '默认的订单回调地址',     // 你也可以在下单时单独设置来想覆盖它
];
$app = \Maowenke\pay\Pay::wechat($config);
$isContract = true;

$result = $app->unify(
'20150806125346',//订单号
0.88,//金额，单位元
'https://pay.weixin.qq.com/wxpay/pay.action',// 支付结果通知网址
//其他不同的参数
[
    'body' => '腾讯充值中心-QQ会员充值',//描述
    'spbill_create_ip' => '123.12.12.123', // 可选，如不传该参数，SDK 将会自动获取相应 IP 地址
    'trade_type' => 'JSAPI', // 请对应换成你的支付方式对应的值类型JSAPI/APP/MWEB
    'openid' => 'oUpF8uMuAJO_M2pxb1Q9zNjWeS6o',//选填openid
    'code'=>'',//trade_type=JSAPI并没有opemid时
    'pay_type'=>"h5",//trade_type=JSAPI并没有openid时,这个确认时公众号还是小程序，有两个值（h5/applet）

    'plan_id' => 123,// 协议模板id
    'contract_code' => 100001256,// 签约协议号
    'contract_display_account' => '腾讯充值中心',// 签约用户的名称
    'contract_notify_url' => 'http://easywechat.org/contract_notify'
]);
```
##支付宝官方支付

```aidl
$config = [
    // 必要配置
    'app_id'             => 'xxxx',//请填写您的AppId，例如：2019******440152
    'merchantPrivateKey'             => 'xxxx',//请填写您的应用私钥，例如：MIIEvQIBADANB
    'alipayCertPath'             => 'xxxx',//请填写您的支付宝公钥证书文件路径
    'alipayRootCertPath'             => 'xxx',//请填写您的支付宝根证书文件路径
    'merchantCertPath '                => 'xxxx',   //请填写您的应用公钥证书文件路径
    'notifyUrl'                => 'xxxx',   //请填写您的支付类接口异步通知接收服务地址
    'encryptKey'                => 'xxxx',   //请填写您的AES密钥
];

$result = $app->unify(
'20150806125346',//订单号
0.88,//金额，单位元
'https://pay.weixin.qq.com/wxpay/pay.action',// 支付结果通知网址
//其他不同的参数
[
    'body' => '腾讯充值中心-QQ会员充值',//描述
    'buyer_id'='',//用户id
]);
```


##第三方支付
``````
$config = [
    'objType'=>'hu',//三方名称
    'payType'=>'Charge',//实例化的类PayTrilateral=支付类，Charge=支付对象类
    'divide'=>false,//是否分账
    'config'=>[//其他单独配置
        
    ]
]
$obj = Pay::trilateral($config);
if($obj===false){
    echo "实例化失败";exit;
}
$unify = [
   'type'=>1,//支付对象类型,1=企业,2=个人
];
$response = $obj->createCharge($unify);