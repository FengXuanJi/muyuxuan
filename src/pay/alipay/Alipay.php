<?php


namespace Maowenke\pay\alipay;


use Alipay\EasySDK\Kernel\Factory;
use Maowenke\pay\Other;

trait Alipay
{
    use Other;

    /**
     * 支付宝官方支付下单
     * @param array $data
     * @return array|false
     */
    protected function MyAliPay(array $data){
        if(empty($data['body'])){
            $this->message = 'body必须设置';
            return false;
        }
        try{
            $result = Factory::payment()->common();
            if(!empty($data['notify_url'])){
                $result = $result->asyncNotify($data['notify_url']);
            }
            $result = $result->create($data['body'],$data['out_trade_no'],$data['total_fee'],empty($data['buyer_id'])?"":$data['buyer_id']);
        }catch (\Exception $e){
            $this->message = $e->getMessage();
            return false;
        }
        if(!empty($result->code) && $result->code == 10000){
            return $result->toMap();
        }else{
            $this->message = $result->msg;
            return false;
        }
    }
}