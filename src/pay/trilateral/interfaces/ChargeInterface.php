<?php


namespace Maowenke\pay\trilateral\interfaces;


interface ChargeInterface
{
    /**创建支付对象
     * @param array $data
     * @return mixed
     */
    public function createCharge(array $data);
}