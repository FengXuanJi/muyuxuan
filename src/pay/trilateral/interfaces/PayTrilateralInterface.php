<?php


namespace Maowenke\pay\trilateral\interfaces;

//Interface
interface PayTrilateralInterface
{
    /**普通支付
     * @param array $data
     * @return mixed
     */
    public function TrilateralPay(array $data);

    /**
     * 分账支付
     * @param array $data
     * @return mixed
     */
    public function TrilateralDividePay(array $data);
}