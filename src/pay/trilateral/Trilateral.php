<?php


namespace Maowenke\pay\trilateral;


use Maowenke\pay\Other;

trait Trilateral
{
    use Other;
    //三方支付入口
    public function TrilateralUnify(array $data){
        if(isset($this->config['divide'])&&$this->config['divide']){
            if(empty($data['divide'])){
                $this->message = "分账信息没有设置";
                return false;
            }
            return $this->TrilateralDividePay($data);
        }else{
            return $this->TrilateralPay($data);
        }
    }
    //普通支付
    protected function TrilateralPay(array $data){
        if(!method_exists($this->PayObj,'TrilateralPay')){
            $this->message = '方法未配置';
            return false;
        }
        return $this->PayObj->TrilateralPay($data);
    }
    //分账支付
    protected function TrilateralDividePay(array $data){
        if(!method_exists($this->PayObj,'TrilateralDividePay')){
            $this->message = '方法未配置';
            return false;
        }
        return $this->PayObj->TrilateralDividePay($data);
    }
}