<?php
namespace Maowenke\pay;
trait Other{
    protected $message = '';
    public function getMessage(){
        return $this->message;
    }
    /**验证配置
     * @param array $verify 配置项
     * @param array $config 新配置
     * @return bool
     */
    protected function checking(array $verify=[],$config=[]){
        if(empty($config)){
            $bool = true;
            foreach ($verify as $v){
                if(empty($this->config[$v])){
                    $this->message = $v."是必传数据";
                    $bool = false;
                    break;
                }
            }
            return $bool;
        }else{
            $bool = true;
            foreach ($verify as $v){
                if(empty($config[$v])){
                    $this->message = $v."是必传数据";
                    $bool = false;
                    break;
                }
            }
            return $bool;
        }
    }

    /**
     * 通过字符串获取类
     * @param string $str
     * @param array $config
     * @return false|mixed
     */
    protected function StrinToObj(string $str,array $config=[]){
        try {
            $reflect = new \ReflectionClass($str);
        }catch (\Exception $e){
            return false;
        }
        return new $reflect->name($config);
    }
}