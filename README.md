# 个人composer库

#### 介绍
个人使用的composer库，  
每个文件下的test.php是实例

#### 安装教程

使用composer安装
```aidl
composer require maowenke/muyuxuan dev-master
```

# 使用说明
##tool 工具
###Mtool 工具类
getDistance 求经纬度距离

##redis redis数据库操作
###distance 距离操作
MredisDistance 距离操作类  
geoAdds 设置二维数组元素  
geoAdd 设置一维数组元素  
geoDist 求两标识的距离  
geoRadius 返回经纬度最大距离的数据  
geoRadiusByMember 求标识经纬度最大距离的数据  
geoDel 删除某个键名  
delKey 删除某个键名

###lock redis锁操作
####MredisLock  redis锁类  
gettLock 获取redis锁  
delLock 删除锁  
delKey 删除某个键名

###ranking redis排行榜
####MredisRanking 排行榜类  
Adds 添加元素多个  
Add 添加元素一个  
getRanking 获取排行榜前$count位  
getMemberRanking 获取用户的排行  
getMemberRange 获取用户附近的用户  
delKey 删除某个键名

###http curl请求
####Http curl请求类
post curl的post请求  
get curl的get请求  
sendRequest curl的请求  
sendAsyncRequest 异步发送请求




